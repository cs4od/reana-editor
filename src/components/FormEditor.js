import React, {useState} from 'react';
import {FormProvider, useForm} from "react-hook-form"
import MultiSelectField from "./fields/MultiSelectField";
import ObjectField from "./fields/ObjectField";
import ArrayField from "./fields/ArrayField";
import {Button} from "@mui/material"
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import SelectField from "./fields/SelectField";
import AutoAwesomeMotionIcon from '@mui/icons-material/AutoAwesomeMotion';
import SimpleTextField from "./fields/SimpleTextField";

const FormEditor = ({
                        yamlData,
                        onSave,
                        files,
                        environments,
                        onCancel,
                        disabled,
                        confirmButtonMessage,
                        initialFilename
                    }) => {
    const [steps, setSteps] = useState(yamlData['workflow']['specification']['steps'])
    const parameters = Object.entries(yamlData['inputs']['parameters']).map(([name, value]) => ({name, value}));
    const commands = []
    for (let i = 0; i < yamlData['workflow']['specification']['steps'].length; i++) {
        commands.push({
            commands: [yamlData['workflow']['specification']['steps'][i]['commands']],
            environment: yamlData['workflow']['specification']['steps'][i]['environment']
        })
    }

    const defaultValues = {
        title: initialFilename ? initialFilename : "",
        inputs: {
            files: yamlData['inputs']['files'], parameters: parameters
        },
        steps: commands,
        outputs: {
            files: yamlData['outputs']['files']
        },
        type: yamlData['workflow']['type'],
        version: yamlData['version']
    }

    const {handleSubmit, control, formState, setValue, getValues, unregister} = useForm({mode: "all", defaultValues})
    // formState is a proxy object. To get up-to-date values, we have to read the values from it before using it:
    // https://github.com/react-hook-form/react-hook-form/issues/1146#issuecomment-612246409
    const {isDirty, isValid} = formState

    const handleSave = data => {
        const parameters = {}
        for (let i = 0; i < data.inputs.parameters.length; i++) {
            parameters[data.inputs.parameters[i].name] = data.inputs.parameters[i].value
        }

        const steps = []
        for (let i = 0; i < data.steps.length; i++) {
            const step = data.steps[i]
            step['commands'][0] = step['commands'][0][0]
            steps.push(step)
        }

        const workflow = {
            inputs: {
                files: data.inputs.files,
                parameters: parameters
            },
            workflow: {
                specification: {
                    steps: steps
                },
                type: data.type
            },
            outputs: {
                files: data.outputs.files
            },
            version: data.version
        }
        onSave(workflow, data.title)
    };

    const addStep = (index) => {
        setValue(`steps[${index}].commands`, ['']);
        setValue(`steps[${index}].environment`, environments[0]);
        setSteps([...steps, 0])
    }

    const removeStep = (index) => {
        for (let i = index; i < steps.length - 1; i++) {
            setValue(`steps[${i}].commands`, getValues(`steps[${i + 1}].commands`));
            setValue(`steps[${i}].environment`, getValues(`steps[${i + 1}].environment`));
        }
        unregister(`steps[${steps.length - 1}].commands`);
        unregister(`steps[${steps.length - 1}].environment`);
        const newArray = [...steps]; // Create a copy of the original array
        newArray.splice(index, 1); // Remove the element at the given index
        setSteps(newArray); // Update the state with the new array
    }

    return (<FormProvider control={control}>
        <form onSubmit={handleSubmit(handleSave)} className="form__wrapper">
            {!disabled && (<SimpleTextField disabled={disabled} safeInput={true}/>)}
            <MultiSelectField options={files} disabled={disabled}/>
            <ObjectField disabled={disabled}/>
            <div className="form-field__wrapper">
                <div className="form-field__label">
                    <AutoAwesomeMotionIcon/>
                    <h3>Steps</h3>
                </div>
                <div className="form-field__content simple-form-field__content">
                    {steps.map((step, i) => (
                        <div key={`step-${i}`}
                             style={{'width': '100%', 'position': 'relative', 'marginBottom': '24px'}}>
                            <Button style={{'position': 'absolute', 'top': '10px', 'right': '0'}}
                                    onClick={() => removeStep(i)} disabled={steps.length < 2 || disabled}
                            >
                                <ClearIcon/>
                            </Button>
                            <ArrayField name={`steps[${i}].commands`} field="Commands" label="Command"
                                        disabled={disabled}/>
                            <SelectField name={`steps[${i}].environment`} options={environments} margin={false}
                                         disabled={disabled}/>
                        </div>
                    ))}
                    <div key={`add-step`} style={{'width': '100%', 'display': 'flex', 'justifyContent': 'center'}}>
                        <Button onClick={() => addStep(steps.length)} disabled={disabled}
                        >
                            <AddIcon/>
                        </Button>
                    </div>
                </div>
            </div>
            <SimpleTextField name={`type`} field="Workflow type" safeInput={true} disabled={disabled}/>
            <ArrayField disabled={disabled}/>
            <SimpleTextField name={`version`} field="Version" safeInput={false} disabled={disabled}/>
            {!disabled && (
                <>
                    <Button
                        type="submit"
                        variant="contained"
                        disabled={!isDirty || !isValid}
                    >
                        {confirmButtonMessage ? confirmButtonMessage : 'Submit'}
                    </Button>
                    <Button
                        onClick={onCancel}
                        variant="contained"
                        style={{'marginLeft': '8px'}}
                    >
                        Cancel
                    </Button>
                </>
            )}

        </form>
    </FormProvider>);
};

export default FormEditor;
