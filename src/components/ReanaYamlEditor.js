import PropTypes from "prop-types"
import yaml from "js-yaml";
import FormEditor from "./FormEditor";
import TextEditor from "./TextEditor";
import React, {useState} from 'react';
import {Button} from "@mui/material";

function ReanaYamlEditor({
                             yamlData,
                             initialFilename,
                             files,
                             environments,
                             handleSave,
                             handleCancel,
                             disabled,
                             confirmButtonMessage
                         }) {
    const [activeTab, setActiveTab] = useState('form');

    const getReanaNewFile = () => {
        return "inputs:\n" +
            "  files:\n" +
            `  - ${files[0]}\n` +
            "  parameters:\n" +
            "    example: \n" +
            "outputs:\n" +
            "  files:\n" +
            "  - \n" +
            "version: 0.0.1\n" +
            "workflow:\n" +
            "  specification:\n" +
            "    steps:\n" +
            "    - commands:\n" +
            "      - \n" +
            "      environment: python:3-slim-bullseye\n" +
            "  type: serial\n"
    }

    const handleTabChange = (tab) => {
        setActiveTab(tab);
    };

    const parsedData = yamlData ? yaml.load(yamlData, {}) : yaml.load(getReanaNewFile(), {});

    return (
        <div className="reana-editor-wrapper">
            <div>
                <Button onClick={() => handleTabChange('form')} variant="contained">Form Editor</Button>
                <Button onClick={() => handleTabChange('text')} style={{'marginLeft': '8px'}}
                        variant="contained">Text Editor</Button>
            </div>
            {activeTab === 'form' && (
                <div>
                    <h2>Form Editor</h2>
                    <FormEditor yamlData={parsedData} files={files} environments={environments}
                                onSave={handleSave} onCancel={handleCancel} disabled={disabled}
                                confirmButtonMessage={confirmButtonMessage} initialFilename={initialFilename}/>
                </div>
            )}

            {activeTab === 'text' && (
                <div>
                    <h2>Text Editor</h2>
                    <TextEditor yamlData={parsedData} onSave={handleSave} onCancel={handleCancel}
                                disabled={disabled} confirmButtonMessage={confirmButtonMessage}
                                initialFilename={initialFilename}/>
                </div>
            )}
        </div>
    );
}

export default ReanaYamlEditor;

ReanaYamlEditor.propTypes = {
    files: PropTypes.array,
    initialFilename: PropTypes.string,
    environments: PropTypes.array,
    handleSave: PropTypes.func,
    handleCancel: PropTypes.func,
    disabled: PropTypes.bool,
    confirmButtonMessage: PropTypes.string
}
