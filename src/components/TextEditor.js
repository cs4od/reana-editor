import React, {useState} from 'react';
import {highlight, languages} from 'prismjs/components/prism-core';
import 'prismjs/components/prism-yaml';
import SimpleCodeEditor from 'react-simple-code-editor';
import {Button, TextField} from "@mui/material";

const TextEditor = ({yamlData, onSave, onCancel, disabled, confirmButtonMessage, initialFilename}) => {
    const [filename, setFilename] = useState(initialFilename);
    const [editedData, setEditedData] = useState(JSON.stringify(yamlData, null, 2));

    const handleInputChange = (event) => {
        setFilename(event.target.value);
    };

    return (
        <div>
            {!disabled && <><label htmlFor="file-title">Filename&nbsp;</label><TextField
                value={filename}
                onChange={handleInputChange}
                size="small"
                slotprops={{
                    textarea: {
                        id: 'file-title',
                    }
                }}
            /></>}
            <SimpleCodeEditor
                value={editedData}
                onValueChange={(value) => {
                    setEditedData(value);
                }}
                highlight={(code) => highlight(code, languages.yaml, 'yaml')}
                padding={10}
                style={{
                    fontFamily: '"Fira code", "Fira Mono", monospace',
                    fontSize: 14,
                    minHeight: 200,
                    border: '1px solid #d9d9d9',
                    borderRadius: 4,
                }}
                disabled={disabled}
            />
            {!disabled && (<>
                <Button onClick={() => onSave(JSON.parse(editedData), filename)} variant="contained"
                        style={{'marginTop': '16px'}}>{confirmButtonMessage ? confirmButtonMessage : 'Submit'}</Button>
                <Button onClick={onCancel} variant="contained"
                        style={{'marginTop': '16px', 'marginLeft': '8px'}}>Cancel</Button></>)}
        < /div>
    );
};

export default TextEditor;
