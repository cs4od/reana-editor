import React from "react"
import {Alert, TextField} from "@mui/material"
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks"
import {Controller} from "react-hook-form"
import PropTypes from "prop-types"

export default function SimpleTextField({disabled, name, safeInput, field}) {
    const pattern = /^[!"#$%&'()*+,\-./:;<=>?@_`{|}~\\\][^]+$/

    const handleKeyPress = e => {
        if (pattern.test(e.key)) e.preventDefault()
    }

    const handlePaste = e => {
        const paste = (e.clipboardData || window.clipboardData).getData("Text")
        let invalid = false
        for (let index = 0; index < paste.length; index++) {
            const char = paste[index]
            if (pattern.test(char)) invalid = true
        }
        if (invalid) e.preventDefault()
    }

    return (
        <Controller
            name={name ? name : "title"}
            render={({field: {onChange, value}, fieldState: {error}}) => (
                <div className="form-field__wrapper">
                    <div className="form-field__label">
                        <LibraryBooksIcon/>
                        <h3>{field ? field : "Title"}</h3>
                    </div>
                    <div className="form-field__content simple-form-field__content">
                        <TextField
                            label={field ? field : "Title"}
                            value={value}
                            onChange={onChange}
                            error={!!error}
                            disabled={disabled}
                            fullWidth
                            size="small"
                            onKeyDown={safeInput ? handleKeyPress : ()=>{}}
                            onPaste={safeInput ? handlePaste : ()=>{}}
                        />
                        <div style={{width: "100%"}}>{error && <Alert severity="error">{error.message}</Alert>}</div>
                    </div>
                </div>
            )}
            rules={{
                minLength: 1,
                maxLength: {value: 80, message: `${field ? field : "Title"} cannot be longer than 80 characters`}
            }}
        />
    )
}

SimpleTextField.propTypes = {
    disabled: PropTypes.bool,
    name: PropTypes.string,
    safeInput: PropTypes.bool,
    field: PropTypes.string
}
