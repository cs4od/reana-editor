import React from "react"
import {Alert, ListItemText, MenuItem, OutlinedInput, Select} from "@mui/material"

import ComputerIcon from "@mui/icons-material/Computer"
import {Controller} from "react-hook-form"
import PropTypes from "prop-types"

export default function SelectField({name, options, disabled, margin}) {
    return (
        <Controller
            name={name}
            render={({field: {onChange, value: environment}, fieldState: {error}}) => {
                return (
                    <div className="form-field__wrapper" style={margin ? {} : {'marginTop': '0'}}>
                        <div className="form-field__label">
                            <ComputerIcon/>
                            <h3>Environment</h3>
                        </div>
                        <div className="form-field__content simple-form-field__content">
                            <Select
                                value={environment}
                                fullWidth
                                onChange={onChange}
                                input={<OutlinedInput label="Select environment"/>}
                                disabled={disabled}
                            >
                                {options?.map(environment => (
                                    <MenuItem key={environment} value={environment}>
                                        <ListItemText primary={environment}/>
                                    </MenuItem>
                                ))}
                            </Select>
                            <div style={{width: "100%"}}>{error &&
                                <Alert severity="error">{error.message}</Alert>}</div>
                        </div>
                    </div>
                )
            }}
            rules={{required: "An environment is required"}}
        />
    )
}

SelectField.propTypes = {
    options: PropTypes.array,
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    margin: PropTypes.bool
}
