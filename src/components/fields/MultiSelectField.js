import React from "react"
import { Alert, Checkbox, ListItemText, MenuItem, OutlinedInput, Select } from "@mui/material"

import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile"
import { Controller } from "react-hook-form"
import PropTypes from "prop-types"

export default function MultiSelectField({ options, disabled }) {
  return (
    <Controller
      name="inputs.files"
      render={({ field: { onChange, value: files }, fieldState: { error } }) => {
        return (
          <div className="form-field__wrapper">
            <div className="form-field__label">
              <InsertDriveFileIcon />
              <h3>Input Files</h3>
            </div>
            <div className="form-field__content simple-form-field__content">
              <Select
                multiple
                value={files}
                fullWidth
                onChange={onChange}
                input={<OutlinedInput label="Select files" />}
                renderValue={selected => selected.join(", ")}
                disabled={disabled}
              >
                {options?.map(file => (
                  <MenuItem key={file} value={file}>
                    <Checkbox checked={files.indexOf(file) > -1} />
                    <ListItemText primary={file} />
                  </MenuItem>
                ))}
              </Select>
              <div style={{ width: "100%" }}>{error && <Alert severity="error">{error.message}</Alert>}</div>
            </div>
          </div>
        )
      }}
      rules={{ required: "An input file is required" }}
    />
  )
}

MultiSelectField.propTypes = {
  options: PropTypes.array,
  disabled: PropTypes.bool
}
