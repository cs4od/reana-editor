import React from "react"
import { Alert, IconButton, TextField } from "@mui/material"

import AbcIcon from "@mui/icons-material/Abc"
import DeleteIcon from "@mui/icons-material/Delete"
import AddIcon from "@mui/icons-material/Add"
import { Controller } from "react-hook-form"
import PropTypes from "prop-types"

export default function ObjectField({ disabled }) {
  const addParameter = (parameters, i) => {
    return [
      ...parameters.slice(0, i + 1),
      {
        name: "",
        value: ""
      },
      ...parameters.slice(i + 1)
    ]
  }

  const removeParameter = (parameters, index) => parameters.filter((_, i) => i !== index)

  const editParameter = (parameters, index, event) =>
    parameters.map((parameter, i) => {
      if (i !== index) {
        return parameter
      } else {
        return { ...parameter, [event.target.name]: event.target.value }
      }
    })

  return (
    <Controller
      name="inputs.parameters"
      render={({ field: { onChange, value: parameters }, fieldState: { error } }) => {
        return (
          <div className="form-field__wrapper">
            <div className="form-field__label">
              <AbcIcon />
              <h3>Parameters</h3>
            </div>
            <div className="form-field__content">
              <div className="form-field__list">
                {parameters?.map((parameter, index) => {
                  return (
                    <div key={index} className={`form-field__list__item object-field__list__item`}>
                      <TextField
                        error={parameter.name.length < 1 && !!error}
                        label="Name"
                        size="small"
                        name="name"
                        required
                        disabled={disabled}
                        value={parameter.name}
                        onChange={event => onChange(editParameter(parameters, index, event))}
                      />
                      <TextField
                        error={parameter.value?.length < 1 && !!error}
                        label="Value"
                        size="small"
                        name="value"
                        required
                        disabled={disabled}
                        value={parameter.value || ""}
                        onChange={event => onChange(editParameter(parameters, index, event))}
                      />
                      <IconButton
                        onClick={() => onChange(removeParameter(parameters, index))}
                        disabled={parameters.length === 1 || disabled}
                        style={{ alignItems: "center" }}
                      >
                        <DeleteIcon />
                      </IconButton>
                      <IconButton disabled={disabled} onClick={() => onChange(addParameter(parameters, index))}>
                        <AddIcon />
                      </IconButton>
                      {(parameter.name.length < 1 || parameter.value?.length < 1) && error && (
                        <Alert severity="error">{error.message}</Alert>
                      )}
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        )
      }}
      rules={{
        validate: value => {
          return (
            value?.every(parameter => parameter.name.length > 0 && parameter.value?.length > 0) ||
            "Parameter is required"
          )
        }
      }}
    />
  )
}

ObjectField.propTypes = {
  disabled: PropTypes.bool
}
