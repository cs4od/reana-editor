import React from "react"
import { Alert, IconButton, TextField } from "@mui/material"

import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile"
import DeleteIcon from "@mui/icons-material/Delete"
import NoteAddIcon from "@mui/icons-material/NoteAdd"
import AddIcon from "@mui/icons-material/Add"
import CodeIcon from "@mui/icons-material/Code"
import { Controller } from "react-hook-form"
import PropTypes from "prop-types"

export default function ArrayField({ name, field, label, disabled }) {
  const addFile = (files, i) => {
    return [...files.slice(0, i + 1), "", ...files.slice(i + 1)]
  }

  const removeFile = (files, index) => files.filter((_, i) => i !== index)

  const editFile = (files, index, event) =>
    files.map((file, i) => {
      if (i !== index) {
        return file
      } else {
        return event.target.value
      }
    })

  return (
    <Controller
      name={name ? name : "outputs.files"}
      render={({ field: { onChange, value: files }, fieldState: { error } }) => {
        return (
          <div className="form-field__wrapper">
            <div className="form-field__label">
              {name ? <CodeIcon /> : <InsertDriveFileIcon />}
              <h3>{field ? field : "Outputs"}</h3>
            </div>
            <div className="form-field__content">
              <div className="form-field__list">
                {files?.map((file, index) => {
                  return (
                    <div key={index} className={`form-field__list__item array-field__list__item`}>
                      <TextField
                        error={file?.length < 1 && !!error}
                        label={label ? label : "Filename"}
                        size="small"
                        required
                        value={file ? file : ""}
                        disabled={disabled}
                        onChange={event => onChange(editFile(files, index, event))}
                      />
                      <IconButton
                        onClick={() => onChange(removeFile(files, index))}
                        disabled={files?.length === 1 || disabled}
                        style={{ alignItems: "center" }}
                      >
                        <DeleteIcon />
                      </IconButton>
                      <IconButton disabled={disabled} onClick={() => onChange(addFile(files, index))}>
                        {name ? <AddIcon /> : <NoteAddIcon />}
                      </IconButton>
                      {file?.length < 1 && error && <Alert severity="error">{error.message}</Alert>}
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        )
      }}
      rules={{
        validate: value => {
          return value?.every(file => file?.length > 0) || "An Output file name is required"
        }
      }}
    />
  )
}

ArrayField.propTypes = {
  name: PropTypes.string,
  field: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool
}
