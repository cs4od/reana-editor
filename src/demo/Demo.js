import React, {useState} from 'react';
import {ReanaYamlEditor} from '../components';
import YAML from "js-yaml";
import classes from './Demo.module.scss'
import {saveAs} from 'file-saver';
import {adaptV4Theme, createTheme, StyledEngineProvider, ThemeProvider} from "@mui/material/styles";

const Demo = () => {
    const [yamlData, setYamlData] = useState(null);

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();

        reader.onload = (e) => {
            const content = e.target.result;
            setYamlData(content);
        };

        reader.readAsText(file);
    };

    const handleSave = (data, filename) => {
        // Convert data to YAML format and save the file
        const yamlContent = YAML.dump(data);
        const blob = new Blob([yamlContent], {type: 'text/yaml;charset=utf-8'});
        alert(`Downloading file ${filename}`)
        saveAs(blob, 'edited_yaml.yaml');
    };


    const theme = createTheme(adaptV4Theme({}))

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
                <div className={classes.wrapper}>
                    <div className={classes.section}>
                        <h1>Reana Yaml Editor Demo</h1>
                        <input type="file" onChange={handleFileChange} style={{'width': '200px'}}/>
                    </div>
                    <div className={classes.section} style={{'marginTop': '32px'}}>
                        <ReanaYamlEditor yamlData={yamlData}
                                         files={["dataset.csv", "data.xls", "sample.txt", "content.yaml"]}
                                         environments={["python:3-slim-bullseye", "debian:11-slim", "ubuntu:mantic", "mongo:nanoserver-1809\n"]}
                                         handleSave={handleSave} handleCancel={() => alert("canceled")}/>
                    </div>
                </div>
            </ThemeProvider>
        </StyledEngineProvider>);
};

export default Demo;
