import React from 'react';
import ReactDOM from 'react-dom';
import Demo from './demo/Demo';
import './demo/index.scss'

ReactDOM.render(<Demo />, document.getElementById('root'));
