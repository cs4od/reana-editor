"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactHookForm = require("react-hook-form");
var _MultiSelectField = _interopRequireDefault(require("./fields/MultiSelectField"));
var _ObjectField = _interopRequireDefault(require("./fields/ObjectField"));
var _ArrayField = _interopRequireDefault(require("./fields/ArrayField"));
var _material = require("@mui/material");
var _Add = _interopRequireDefault(require("@mui/icons-material/Add"));
var _Clear = _interopRequireDefault(require("@mui/icons-material/Clear"));
var _SelectField = _interopRequireDefault(require("./fields/SelectField"));
var _AutoAwesomeMotion = _interopRequireDefault(require("@mui/icons-material/AutoAwesomeMotion"));
var _SimpleTextField = _interopRequireDefault(require("./fields/SimpleTextField"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var FormEditor = function FormEditor(_ref) {
  var yamlData = _ref.yamlData,
    onSave = _ref.onSave,
    files = _ref.files,
    environments = _ref.environments,
    onCancel = _ref.onCancel,
    disabled = _ref.disabled,
    confirmButtonMessage = _ref.confirmButtonMessage,
    initialFilename = _ref.initialFilename;
  var _useState = (0, _react.useState)(yamlData['workflow']['specification']['steps']),
    _useState2 = _slicedToArray(_useState, 2),
    steps = _useState2[0],
    setSteps = _useState2[1];
  var parameters = Object.entries(yamlData['inputs']['parameters']).map(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 2),
      name = _ref3[0],
      value = _ref3[1];
    return {
      name: name,
      value: value
    };
  });
  var commands = [];
  for (var i = 0; i < yamlData['workflow']['specification']['steps'].length; i++) {
    commands.push({
      commands: [yamlData['workflow']['specification']['steps'][i]['commands']],
      environment: yamlData['workflow']['specification']['steps'][i]['environment']
    });
  }
  var defaultValues = {
    title: initialFilename ? initialFilename : "",
    inputs: {
      files: yamlData['inputs']['files'],
      parameters: parameters
    },
    steps: commands,
    outputs: {
      files: yamlData['outputs']['files']
    },
    type: yamlData['workflow']['type'],
    version: yamlData['version']
  };
  var _useForm = (0, _reactHookForm.useForm)({
      mode: "all",
      defaultValues: defaultValues
    }),
    handleSubmit = _useForm.handleSubmit,
    control = _useForm.control,
    formState = _useForm.formState,
    setValue = _useForm.setValue,
    getValues = _useForm.getValues,
    unregister = _useForm.unregister;
  // formState is a proxy object. To get up-to-date values, we have to read the values from it before using it:
  // https://github.com/react-hook-form/react-hook-form/issues/1146#issuecomment-612246409
  var isDirty = formState.isDirty,
    isValid = formState.isValid;
  var handleSave = function handleSave(data) {
    var parameters = {};
    for (var _i2 = 0; _i2 < data.inputs.parameters.length; _i2++) {
      parameters[data.inputs.parameters[_i2].name] = data.inputs.parameters[_i2].value;
    }
    var steps = [];
    for (var _i3 = 0; _i3 < data.steps.length; _i3++) {
      var step = data.steps[_i3];
      step['commands'][0] = step['commands'][0][0];
      steps.push(step);
    }
    var workflow = {
      inputs: {
        files: data.inputs.files,
        parameters: parameters
      },
      workflow: {
        specification: {
          steps: steps
        },
        type: data.type
      },
      outputs: {
        files: data.outputs.files
      },
      version: data.version
    };
    onSave(workflow, data.title);
  };
  var addStep = function addStep(index) {
    setValue("steps[".concat(index, "].commands"), ['']);
    setValue("steps[".concat(index, "].environment"), environments[0]);
    setSteps([].concat(_toConsumableArray(steps), [0]));
  };
  var removeStep = function removeStep(index) {
    for (var _i4 = index; _i4 < steps.length - 1; _i4++) {
      setValue("steps[".concat(_i4, "].commands"), getValues("steps[".concat(_i4 + 1, "].commands")));
      setValue("steps[".concat(_i4, "].environment"), getValues("steps[".concat(_i4 + 1, "].environment")));
    }
    unregister("steps[".concat(steps.length - 1, "].commands"));
    unregister("steps[".concat(steps.length - 1, "].environment"));
    var newArray = _toConsumableArray(steps); // Create a copy of the original array
    newArray.splice(index, 1); // Remove the element at the given index
    setSteps(newArray); // Update the state with the new array
  };

  return /*#__PURE__*/_react.default.createElement(_reactHookForm.FormProvider, {
    control: control
  }, /*#__PURE__*/_react.default.createElement("form", {
    onSubmit: handleSubmit(handleSave),
    className: "form__wrapper"
  }, !disabled && /*#__PURE__*/_react.default.createElement(_SimpleTextField.default, {
    disabled: disabled,
    safeInput: true
  }), /*#__PURE__*/_react.default.createElement(_MultiSelectField.default, {
    options: files,
    disabled: disabled
  }), /*#__PURE__*/_react.default.createElement(_ObjectField.default, {
    disabled: disabled
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field__wrapper"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field__label"
  }, /*#__PURE__*/_react.default.createElement(_AutoAwesomeMotion.default, null), /*#__PURE__*/_react.default.createElement("h3", null, "Steps")), /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field__content simple-form-field__content"
  }, steps.map(function (step, i) {
    return /*#__PURE__*/_react.default.createElement("div", {
      key: "step-".concat(i),
      style: {
        'width': '100%',
        'position': 'relative',
        'marginBottom': '24px'
      }
    }, /*#__PURE__*/_react.default.createElement(_material.Button, {
      style: {
        'position': 'absolute',
        'top': '10px',
        'right': '0'
      },
      onClick: function onClick() {
        return removeStep(i);
      },
      disabled: steps.length < 2 || disabled
    }, /*#__PURE__*/_react.default.createElement(_Clear.default, null)), /*#__PURE__*/_react.default.createElement(_ArrayField.default, {
      name: "steps[".concat(i, "].commands"),
      field: "Commands",
      label: "Command",
      disabled: disabled
    }), /*#__PURE__*/_react.default.createElement(_SelectField.default, {
      name: "steps[".concat(i, "].environment"),
      options: environments,
      margin: false,
      disabled: disabled
    }));
  }), /*#__PURE__*/_react.default.createElement("div", {
    key: "add-step",
    style: {
      'width': '100%',
      'display': 'flex',
      'justifyContent': 'center'
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: function onClick() {
      return addStep(steps.length);
    },
    disabled: disabled
  }, /*#__PURE__*/_react.default.createElement(_Add.default, null))))), /*#__PURE__*/_react.default.createElement(_SimpleTextField.default, {
    name: "type",
    field: "Workflow type",
    safeInput: true,
    disabled: disabled
  }), /*#__PURE__*/_react.default.createElement(_ArrayField.default, {
    disabled: disabled
  }), /*#__PURE__*/_react.default.createElement(_SimpleTextField.default, {
    name: "version",
    field: "Version",
    safeInput: false,
    disabled: disabled
  }), !disabled && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.Button, {
    type: "submit",
    variant: "contained",
    disabled: !isDirty || !isValid
  }, confirmButtonMessage ? confirmButtonMessage : 'Submit'), /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: onCancel,
    variant: "contained",
    style: {
      'marginLeft': '8px'
    }
  }, "Cancel"))));
};
var _default = FormEditor;
exports.default = _default;