"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _prismCore = require("prismjs/components/prism-core");
require("prismjs/components/prism-yaml");
var _reactSimpleCodeEditor = _interopRequireDefault(require("react-simple-code-editor"));
var _material = require("@mui/material");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var TextEditor = function TextEditor(_ref) {
  var yamlData = _ref.yamlData,
    onSave = _ref.onSave,
    onCancel = _ref.onCancel,
    disabled = _ref.disabled,
    confirmButtonMessage = _ref.confirmButtonMessage,
    initialFilename = _ref.initialFilename;
  var _useState = (0, _react.useState)(initialFilename),
    _useState2 = _slicedToArray(_useState, 2),
    filename = _useState2[0],
    setFilename = _useState2[1];
  var _useState3 = (0, _react.useState)(JSON.stringify(yamlData, null, 2)),
    _useState4 = _slicedToArray(_useState3, 2),
    editedData = _useState4[0],
    setEditedData = _useState4[1];
  var handleInputChange = function handleInputChange(event) {
    setFilename(event.target.value);
  };
  return /*#__PURE__*/_react.default.createElement("div", null, !disabled && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "file-title"
  }, "Filename\xA0"), /*#__PURE__*/_react.default.createElement(_material.TextField, {
    value: filename,
    onChange: handleInputChange,
    size: "small",
    slotprops: {
      textarea: {
        id: 'file-title'
      }
    }
  })), /*#__PURE__*/_react.default.createElement(_reactSimpleCodeEditor.default, {
    value: editedData,
    onValueChange: function onValueChange(value) {
      setEditedData(value);
    },
    highlight: function highlight(code) {
      return (0, _prismCore.highlight)(code, _prismCore.languages.yaml, 'yaml');
    },
    padding: 10,
    style: {
      fontFamily: '"Fira code", "Fira Mono", monospace',
      fontSize: 14,
      minHeight: 200,
      border: '1px solid #d9d9d9',
      borderRadius: 4
    },
    disabled: disabled
  }), !disabled && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: function onClick() {
      return onSave(JSON.parse(editedData), filename);
    },
    variant: "contained",
    style: {
      'marginTop': '16px'
    }
  }, confirmButtonMessage ? confirmButtonMessage : 'Submit'), /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: onCancel,
    variant: "contained",
    style: {
      'marginTop': '16px',
      'marginLeft': '8px'
    }
  }, "Cancel")));
};
var _default = TextEditor;
exports.default = _default;