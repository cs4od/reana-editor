"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _propTypes = _interopRequireDefault(require("prop-types"));
var _jsYaml = _interopRequireDefault(require("js-yaml"));
var _FormEditor = _interopRequireDefault(require("./FormEditor"));
var _TextEditor = _interopRequireDefault(require("./TextEditor"));
var _react = _interopRequireWildcard(require("react"));
var _material = require("@mui/material");
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function ReanaYamlEditor(_ref) {
  var yamlData = _ref.yamlData,
    initialFilename = _ref.initialFilename,
    files = _ref.files,
    environments = _ref.environments,
    handleSave = _ref.handleSave,
    handleCancel = _ref.handleCancel,
    disabled = _ref.disabled,
    confirmButtonMessage = _ref.confirmButtonMessage;
  var _useState = (0, _react.useState)('form'),
    _useState2 = _slicedToArray(_useState, 2),
    activeTab = _useState2[0],
    setActiveTab = _useState2[1];
  var getReanaNewFile = function getReanaNewFile() {
    return "inputs:\n" + "  files:\n" + "  - ".concat(files[0], "\n") + "  parameters:\n" + "    example: \n" + "outputs:\n" + "  files:\n" + "  - \n" + "version: 0.0.1\n" + "workflow:\n" + "  specification:\n" + "    steps:\n" + "    - commands:\n" + "      - \n" + "      environment: python:3-slim-bullseye\n" + "  type: serial\n";
  };
  var handleTabChange = function handleTabChange(tab) {
    setActiveTab(tab);
  };
  var parsedData = yamlData ? _jsYaml.default.load(yamlData, {}) : _jsYaml.default.load(getReanaNewFile(), {});
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "reana-editor-wrapper"
  }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: function onClick() {
      return handleTabChange('form');
    },
    variant: "contained"
  }, "Form Editor"), /*#__PURE__*/_react.default.createElement(_material.Button, {
    onClick: function onClick() {
      return handleTabChange('text');
    },
    style: {
      'marginLeft': '8px'
    },
    variant: "contained"
  }, "Text Editor")), activeTab === 'form' && /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("h2", null, "Form Editor"), /*#__PURE__*/_react.default.createElement(_FormEditor.default, {
    yamlData: parsedData,
    files: files,
    environments: environments,
    onSave: handleSave,
    onCancel: handleCancel,
    disabled: disabled,
    confirmButtonMessage: confirmButtonMessage,
    initialFilename: initialFilename
  })), activeTab === 'text' && /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("h2", null, "Text Editor"), /*#__PURE__*/_react.default.createElement(_TextEditor.default, {
    yamlData: parsedData,
    onSave: handleSave,
    onCancel: handleCancel,
    disabled: disabled,
    confirmButtonMessage: confirmButtonMessage,
    initialFilename: initialFilename
  })));
}
var _default = ReanaYamlEditor;
exports.default = _default;
ReanaYamlEditor.propTypes = {
  files: _propTypes.default.array,
  initialFilename: _propTypes.default.string,
  environments: _propTypes.default.array,
  handleSave: _propTypes.default.func,
  handleCancel: _propTypes.default.func,
  disabled: _propTypes.default.bool,
  confirmButtonMessage: _propTypes.default.string
};