"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = SelectField;
var _react = _interopRequireDefault(require("react"));
var _material = require("@mui/material");
var _Computer = _interopRequireDefault(require("@mui/icons-material/Computer"));
var _reactHookForm = require("react-hook-form");
var _propTypes = _interopRequireDefault(require("prop-types"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function SelectField(_ref) {
  var name = _ref.name,
    options = _ref.options,
    disabled = _ref.disabled,
    margin = _ref.margin;
  return /*#__PURE__*/_react.default.createElement(_reactHookForm.Controller, {
    name: name,
    render: function render(_ref2) {
      var _ref2$field = _ref2.field,
        onChange = _ref2$field.onChange,
        environment = _ref2$field.value,
        error = _ref2.fieldState.error;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__wrapper",
        style: margin ? {} : {
          'marginTop': '0'
        }
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__label"
      }, /*#__PURE__*/_react.default.createElement(_Computer.default, null), /*#__PURE__*/_react.default.createElement("h3", null, "Environment")), /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__content simple-form-field__content"
      }, /*#__PURE__*/_react.default.createElement(_material.Select, {
        value: environment,
        fullWidth: true,
        onChange: onChange,
        input: /*#__PURE__*/_react.default.createElement(_material.OutlinedInput, {
          label: "Select environment"
        }),
        disabled: disabled
      }, options === null || options === void 0 ? void 0 : options.map(function (environment) {
        return /*#__PURE__*/_react.default.createElement(_material.MenuItem, {
          key: environment,
          value: environment
        }, /*#__PURE__*/_react.default.createElement(_material.ListItemText, {
          primary: environment
        }));
      })), /*#__PURE__*/_react.default.createElement("div", {
        style: {
          width: "100%"
        }
      }, error && /*#__PURE__*/_react.default.createElement(_material.Alert, {
        severity: "error"
      }, error.message))));
    },
    rules: {
      required: "An environment is required"
    }
  });
}
SelectField.propTypes = {
  options: _propTypes.default.array,
  disabled: _propTypes.default.bool,
  name: _propTypes.default.string.isRequired,
  margin: _propTypes.default.bool
};