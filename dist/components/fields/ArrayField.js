"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ArrayField;
var _react = _interopRequireDefault(require("react"));
var _material = require("@mui/material");
var _InsertDriveFile = _interopRequireDefault(require("@mui/icons-material/InsertDriveFile"));
var _Delete = _interopRequireDefault(require("@mui/icons-material/Delete"));
var _NoteAdd = _interopRequireDefault(require("@mui/icons-material/NoteAdd"));
var _Add = _interopRequireDefault(require("@mui/icons-material/Add"));
var _Code = _interopRequireDefault(require("@mui/icons-material/Code"));
var _reactHookForm = require("react-hook-form");
var _propTypes = _interopRequireDefault(require("prop-types"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function ArrayField(_ref) {
  var name = _ref.name,
    field = _ref.field,
    label = _ref.label,
    disabled = _ref.disabled;
  var addFile = function addFile(files, i) {
    return [].concat(_toConsumableArray(files.slice(0, i + 1)), [""], _toConsumableArray(files.slice(i + 1)));
  };
  var removeFile = function removeFile(files, index) {
    return files.filter(function (_, i) {
      return i !== index;
    });
  };
  var editFile = function editFile(files, index, event) {
    return files.map(function (file, i) {
      if (i !== index) {
        return file;
      } else {
        return event.target.value;
      }
    });
  };
  return /*#__PURE__*/_react.default.createElement(_reactHookForm.Controller, {
    name: name ? name : "outputs.files",
    render: function render(_ref2) {
      var _ref2$field = _ref2.field,
        _onChange = _ref2$field.onChange,
        files = _ref2$field.value,
        error = _ref2.fieldState.error;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__wrapper"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__label"
      }, name ? /*#__PURE__*/_react.default.createElement(_Code.default, null) : /*#__PURE__*/_react.default.createElement(_InsertDriveFile.default, null), /*#__PURE__*/_react.default.createElement("h3", null, field ? field : "Outputs")), /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__content"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__list"
      }, files === null || files === void 0 ? void 0 : files.map(function (file, index) {
        return /*#__PURE__*/_react.default.createElement("div", {
          key: index,
          className: "form-field__list__item array-field__list__item"
        }, /*#__PURE__*/_react.default.createElement(_material.TextField, {
          error: (file === null || file === void 0 ? void 0 : file.length) < 1 && !!error,
          label: label ? label : "Filename",
          size: "small",
          required: true,
          value: file ? file : "",
          disabled: disabled,
          onChange: function onChange(event) {
            return _onChange(editFile(files, index, event));
          }
        }), /*#__PURE__*/_react.default.createElement(_material.IconButton, {
          onClick: function onClick() {
            return _onChange(removeFile(files, index));
          },
          disabled: (files === null || files === void 0 ? void 0 : files.length) === 1 || disabled,
          style: {
            alignItems: "center"
          }
        }, /*#__PURE__*/_react.default.createElement(_Delete.default, null)), /*#__PURE__*/_react.default.createElement(_material.IconButton, {
          disabled: disabled,
          onClick: function onClick() {
            return _onChange(addFile(files, index));
          }
        }, name ? /*#__PURE__*/_react.default.createElement(_Add.default, null) : /*#__PURE__*/_react.default.createElement(_NoteAdd.default, null)), (file === null || file === void 0 ? void 0 : file.length) < 1 && error && /*#__PURE__*/_react.default.createElement(_material.Alert, {
          severity: "error"
        }, error.message));
      }))));
    },
    rules: {
      validate: function validate(value) {
        return (value === null || value === void 0 ? void 0 : value.every(function (file) {
          return (file === null || file === void 0 ? void 0 : file.length) > 0;
        })) || "An Output file name is required";
      }
    }
  });
}
ArrayField.propTypes = {
  name: _propTypes.default.string,
  field: _propTypes.default.string,
  label: _propTypes.default.string,
  disabled: _propTypes.default.bool
};