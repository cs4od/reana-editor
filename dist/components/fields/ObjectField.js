"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ObjectField;
var _react = _interopRequireDefault(require("react"));
var _material = require("@mui/material");
var _Abc = _interopRequireDefault(require("@mui/icons-material/Abc"));
var _Delete = _interopRequireDefault(require("@mui/icons-material/Delete"));
var _Add = _interopRequireDefault(require("@mui/icons-material/Add"));
var _reactHookForm = require("react-hook-form");
var _propTypes = _interopRequireDefault(require("prop-types"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function ObjectField(_ref) {
  var disabled = _ref.disabled;
  var addParameter = function addParameter(parameters, i) {
    return [].concat(_toConsumableArray(parameters.slice(0, i + 1)), [{
      name: "",
      value: ""
    }], _toConsumableArray(parameters.slice(i + 1)));
  };
  var removeParameter = function removeParameter(parameters, index) {
    return parameters.filter(function (_, i) {
      return i !== index;
    });
  };
  var editParameter = function editParameter(parameters, index, event) {
    return parameters.map(function (parameter, i) {
      if (i !== index) {
        return parameter;
      } else {
        return _objectSpread(_objectSpread({}, parameter), {}, _defineProperty({}, event.target.name, event.target.value));
      }
    });
  };
  return /*#__PURE__*/_react.default.createElement(_reactHookForm.Controller, {
    name: "inputs.parameters",
    render: function render(_ref2) {
      var _ref2$field = _ref2.field,
        _onChange = _ref2$field.onChange,
        parameters = _ref2$field.value,
        error = _ref2.fieldState.error;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__wrapper"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__label"
      }, /*#__PURE__*/_react.default.createElement(_Abc.default, null), /*#__PURE__*/_react.default.createElement("h3", null, "Parameters")), /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__content"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__list"
      }, parameters === null || parameters === void 0 ? void 0 : parameters.map(function (parameter, index) {
        var _parameter$value, _parameter$value2;
        return /*#__PURE__*/_react.default.createElement("div", {
          key: index,
          className: "form-field__list__item object-field__list__item"
        }, /*#__PURE__*/_react.default.createElement(_material.TextField, {
          error: parameter.name.length < 1 && !!error,
          label: "Name",
          size: "small",
          name: "name",
          required: true,
          disabled: disabled,
          value: parameter.name,
          onChange: function onChange(event) {
            return _onChange(editParameter(parameters, index, event));
          }
        }), /*#__PURE__*/_react.default.createElement(_material.TextField, {
          error: ((_parameter$value = parameter.value) === null || _parameter$value === void 0 ? void 0 : _parameter$value.length) < 1 && !!error,
          label: "Value",
          size: "small",
          name: "value",
          required: true,
          disabled: disabled,
          value: parameter.value || "",
          onChange: function onChange(event) {
            return _onChange(editParameter(parameters, index, event));
          }
        }), /*#__PURE__*/_react.default.createElement(_material.IconButton, {
          onClick: function onClick() {
            return _onChange(removeParameter(parameters, index));
          },
          disabled: parameters.length === 1 || disabled,
          style: {
            alignItems: "center"
          }
        }, /*#__PURE__*/_react.default.createElement(_Delete.default, null)), /*#__PURE__*/_react.default.createElement(_material.IconButton, {
          disabled: disabled,
          onClick: function onClick() {
            return _onChange(addParameter(parameters, index));
          }
        }, /*#__PURE__*/_react.default.createElement(_Add.default, null)), (parameter.name.length < 1 || ((_parameter$value2 = parameter.value) === null || _parameter$value2 === void 0 ? void 0 : _parameter$value2.length) < 1) && error && /*#__PURE__*/_react.default.createElement(_material.Alert, {
          severity: "error"
        }, error.message));
      }))));
    },
    rules: {
      validate: function validate(value) {
        return (value === null || value === void 0 ? void 0 : value.every(function (parameter) {
          var _parameter$value3;
          return parameter.name.length > 0 && ((_parameter$value3 = parameter.value) === null || _parameter$value3 === void 0 ? void 0 : _parameter$value3.length) > 0;
        })) || "Parameter is required";
      }
    }
  });
}
ObjectField.propTypes = {
  disabled: _propTypes.default.bool
};