"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = SimpleTextField;
var _react = _interopRequireDefault(require("react"));
var _material = require("@mui/material");
var _LibraryBooks = _interopRequireDefault(require("@mui/icons-material/LibraryBooks"));
var _reactHookForm = require("react-hook-form");
var _propTypes = _interopRequireDefault(require("prop-types"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function SimpleTextField(_ref) {
  var disabled = _ref.disabled,
    name = _ref.name,
    safeInput = _ref.safeInput,
    field = _ref.field;
  var pattern = /^[!"#$%&'()*+,\-./:;<=>?@_`{|}~\\\][^]+$/;
  var handleKeyPress = function handleKeyPress(e) {
    if (pattern.test(e.key)) e.preventDefault();
  };
  var handlePaste = function handlePaste(e) {
    var paste = (e.clipboardData || window.clipboardData).getData("Text");
    var invalid = false;
    for (var index = 0; index < paste.length; index++) {
      var char = paste[index];
      if (pattern.test(char)) invalid = true;
    }
    if (invalid) e.preventDefault();
  };
  return /*#__PURE__*/_react.default.createElement(_reactHookForm.Controller, {
    name: name ? name : "title",
    render: function render(_ref2) {
      var _ref2$field = _ref2.field,
        onChange = _ref2$field.onChange,
        value = _ref2$field.value,
        error = _ref2.fieldState.error;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__wrapper"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__label"
      }, /*#__PURE__*/_react.default.createElement(_LibraryBooks.default, null), /*#__PURE__*/_react.default.createElement("h3", null, field ? field : "Title")), /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__content simple-form-field__content"
      }, /*#__PURE__*/_react.default.createElement(_material.TextField, {
        label: field ? field : "Title",
        value: value,
        onChange: onChange,
        error: !!error,
        disabled: disabled,
        fullWidth: true,
        size: "small",
        onKeyDown: safeInput ? handleKeyPress : function () {},
        onPaste: safeInput ? handlePaste : function () {}
      }), /*#__PURE__*/_react.default.createElement("div", {
        style: {
          width: "100%"
        }
      }, error && /*#__PURE__*/_react.default.createElement(_material.Alert, {
        severity: "error"
      }, error.message))));
    },
    rules: {
      minLength: 1,
      maxLength: {
        value: 80,
        message: "".concat(field ? field : "Title", " cannot be longer than 80 characters")
      }
    }
  });
}
SimpleTextField.propTypes = {
  disabled: _propTypes.default.bool,
  name: _propTypes.default.string,
  safeInput: _propTypes.default.bool,
  field: _propTypes.default.string
};