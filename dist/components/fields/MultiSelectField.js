"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = MultiSelectField;
var _react = _interopRequireDefault(require("react"));
var _material = require("@mui/material");
var _InsertDriveFile = _interopRequireDefault(require("@mui/icons-material/InsertDriveFile"));
var _reactHookForm = require("react-hook-form");
var _propTypes = _interopRequireDefault(require("prop-types"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function MultiSelectField(_ref) {
  var options = _ref.options,
    disabled = _ref.disabled;
  return /*#__PURE__*/_react.default.createElement(_reactHookForm.Controller, {
    name: "inputs.files",
    render: function render(_ref2) {
      var _ref2$field = _ref2.field,
        onChange = _ref2$field.onChange,
        files = _ref2$field.value,
        error = _ref2.fieldState.error;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__wrapper"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__label"
      }, /*#__PURE__*/_react.default.createElement(_InsertDriveFile.default, null), /*#__PURE__*/_react.default.createElement("h3", null, "Input Files")), /*#__PURE__*/_react.default.createElement("div", {
        className: "form-field__content simple-form-field__content"
      }, /*#__PURE__*/_react.default.createElement(_material.Select, {
        multiple: true,
        value: files,
        fullWidth: true,
        onChange: onChange,
        input: /*#__PURE__*/_react.default.createElement(_material.OutlinedInput, {
          label: "Select files"
        }),
        renderValue: function renderValue(selected) {
          return selected.join(", ");
        },
        disabled: disabled
      }, options === null || options === void 0 ? void 0 : options.map(function (file) {
        return /*#__PURE__*/_react.default.createElement(_material.MenuItem, {
          key: file,
          value: file
        }, /*#__PURE__*/_react.default.createElement(_material.Checkbox, {
          checked: files.indexOf(file) > -1
        }), /*#__PURE__*/_react.default.createElement(_material.ListItemText, {
          primary: file
        }));
      })), /*#__PURE__*/_react.default.createElement("div", {
        style: {
          width: "100%"
        }
      }, error && /*#__PURE__*/_react.default.createElement(_material.Alert, {
        severity: "error"
      }, error.message))));
    },
    rules: {
      required: "An input file is required"
    }
  });
}
MultiSelectField.propTypes = {
  options: _propTypes.default.array,
  disabled: _propTypes.default.bool
};