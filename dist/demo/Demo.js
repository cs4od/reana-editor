"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _components = require("../components");
var _jsYaml = _interopRequireDefault(require("js-yaml"));
var _DemoModule = _interopRequireDefault(require("./Demo.module.scss"));
var _fileSaver = require("file-saver");
var _styles = require("@mui/material/styles");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var Demo = function Demo() {
  var _useState = (0, _react.useState)(null),
    _useState2 = _slicedToArray(_useState, 2),
    yamlData = _useState2[0],
    setYamlData = _useState2[1];
  var handleFileChange = function handleFileChange(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
      var content = e.target.result;
      setYamlData(content);
    };
    reader.readAsText(file);
  };
  var handleSave = function handleSave(data, filename) {
    // Convert data to YAML format and save the file
    var yamlContent = _jsYaml.default.dump(data, {});
    var blob = new Blob([yamlContent], {
      type: 'text/yaml;charset=utf-8'
    });
    alert("Downloading file ".concat(filename));
    (0, _fileSaver.saveAs)(blob, 'edited_yaml.yaml');
  };
  var theme = (0, _styles.createTheme)((0, _styles.adaptV4Theme)({}));
  return /*#__PURE__*/_react.default.createElement(_styles.StyledEngineProvider, {
    injectFirst: true
  }, /*#__PURE__*/_react.default.createElement(_styles.ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _DemoModule.default.wrapper
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _DemoModule.default.section
  }, /*#__PURE__*/_react.default.createElement("h1", null, "Reana Yaml Editor Demo"), /*#__PURE__*/_react.default.createElement("input", {
    type: "file",
    onChange: handleFileChange,
    style: {
      'width': '200px'
    }
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: _DemoModule.default.section,
    style: {
      'marginTop': '32px'
    }
  }, /*#__PURE__*/_react.default.createElement(_components.ReanaYamlEditor, {
    yamlData: yamlData,
    files: ["dataset.csv", "data.xls", "sample.txt", "content.yaml"],
    environments: ["python:3-slim-bullseye", "debian:11-slim", "ubuntu:mantic", "mongo:nanoserver-1809\n"],
    handleSave: handleSave,
    handleCancel: function handleCancel() {
      return alert("canceled");
    }
  })))));
};
var _default = Demo;
exports.default = _default;