# reana-editor

## Install

In order to use this library in other projects, you can install it in two ways:

- You can just need to add it to your dependencies in the `package.json` file. An example is below:

  ```json
  {
    "dependencies": {
      "reana-editor": "git+https://gitlab.cern.ch/cs4od/reana-editor.git#0.1.5"
    }
  }
  ```

  You replace the value after the `#` with whatever version you're planning to use.

- You can add `.npmrc` file to your project with following content:

  ```txt
  reana-editor:registry=https://cern.ch/cern-nexus/repository/authzsvc-npm/
  ```

  Then you can just use `npm i reana-editor`


## Usage

### CERN toolbar

```jsx
import React, {useState} from 'react';
import {ReanaYamlEditor} from '../components';
import YAML from "js-yaml";
import classes from './Demo.module.scss'
import {saveAs} from 'file-saver';

const Demo = () => {
    const [yamlData, setYamlData] = useState(null);

    const handleSave = (data) => {
        // Convert data to YAML format and save the file
        const yamlContent = YAML.dump(data);
        const blob = new Blob([yamlContent], {type: 'text/yaml;charset=utf-8'});
        saveAs(blob, 'edited_yaml.yaml');
    };

    return (<ReanaYamlEditor yamlData={yamlData} files={["dataset.csv", "data.xls", "sample.txt", "content.yaml"]}
                             environments={["python:3-slim-bullseye", "debian:11-slim", "ubuntu:mantic", "mongo:nanoserver-1809\n"]}
                             handleSave={handleSave}/>);
};

export default Demo;
```

## Author

[The CERN Authorization Service team](https://gitlab.cern.ch/authzsvc)
